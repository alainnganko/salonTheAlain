<?php

namespace Database\Factories;

use Illuminate\Database\Eloquent\Factories\Factory;

class ProduitFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            "idT"=>$this->faker->numberBetween(1,2),
            "nomProduit"=>$this->faker->words(3,true),
            "marque"=>$this->faker->word(),
            "image"=>null,

            "duree"=>"deux ans",
            "estdisponible"=>$this->faker->boolean(),
        ];
    }
}

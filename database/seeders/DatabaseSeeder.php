<?php

namespace Database\Seeders;

use App\Models\Commande;
use App\Models\Grammage;
use App\Models\Inventaire;
use App\Models\LigneCommande;
use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Pays;
use App\Models\Produit;
use App\Models\Role;
use App\Models\Type;
use App\Models\Voyage;
use CreateProduitsTable;
use Illuminate\Hashing\BcryptHasher;
use phpDocumentor\Reflection\PseudoTypes\True_;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        //cette méthode ?
        $role1=Role::create([
            "nomRole"=>"Admin",

        ]);
        $role2=Role::create([
            "nomRole"=>"Usager",
        ]);
$utilisateur=User::create([
    "nomUt"=>"Nganko",
    "prenomUt"=>"Alain",
    "email"=>"alainnganko@pm.me",
    "password"=>bcrypt("130877_Lang"),
    "idR"=>1
]);
        $type1=Type::create([
            "nomType"=>"bio",
            "couleurType"=>"vert",

        ]);
        $type2=Type::create([
            "nomType"=>"detox",
            "couleurType"=>"vert",

        ]);
        $commande=Commande::create([
            "idUt"=>1,
            "dateCom"=>"2022-04-24",
            "etatCom"=>True,
        ]);
        $grammage1=Grammage::create([
            "nbre"=>100,
            "unite"=>"grs"
        ]);
        $grammage2=Grammage::create([
            "nbre"=>80,
            "unite"=>"grs"
        ]);
        $grammage3=Grammage::create([
            "nbre"=>50,
            "unite"=>"grs"
        ]);
        $produit1=Produit::create([
            "idT"=>$type1->idT,
            "nomProduit"=>"The vert jasmin",
            "marque"=>"jasmin",
            "image"=>"thevj.jpg",
            "duree"=>"deux ans",
            "estdisponible"=>True,


        ]);
        $produit2=Produit::create([
            "idT"=>$type1->idT,
            "nomProduit"=>"The vert menthe",
            "marque"=>"menthe",
            "image"=>"thevm.jpg",
            "duree"=>"deux ans",
            "estdisponible"=>True,
        ]);
        $produit3=Produit::create([
            "idT"=>$type2->idT,
            "nomProduit"=>"The Rouge vanille",
            "marque"=>"vanille",
            "image"=>"therv.jpg",

            "duree"=>"deux ans",
            "estdisponible"=>True,
        ]);
        $produit4=Produit::create([
            "idT"=>$type2->idT,
            "nomProduit"=>"The Rouge arome",
            "marque"=>"arome",
            "image"=>"thera.jpg",

            "duree"=>"deux ans",
            "estdisponible"=>True,
        ]);
        $inventaire1=Inventaire::create([
           "idP"=>$produit1->idP,
            "idG"=>$grammage1->idG,
            "prix"=>20.99,
            "stockInv"=>500,
        ]);
        $inventaire2=Inventaire::create([
            "idP"=>$produit1->idP,
            "idG"=>$grammage2->idG,
            "prix"=>14.99,
            "stockInv"=>400,
        ]);
        $inventaire3=Inventaire::create([
            "idP"=>$produit2->idP,
            "idG"=>$grammage3->idG,
            "prix"=>25.99,
            "stockInv"=>300,
        ]);
        $inventaire4=Inventaire::create([
            "idP"=>$produit2->idP,
            "idG"=>$grammage1->idG,
            "prix"=>30.5,
            "stockInv"=>200,
        ]);

 $ligneCommandes=LigneCommande::create([
            "idC"=>1,
            "idI"=>1,
            "quantite"=>20,
        ]);
        $ligneCommandes2=LigneCommande::create([
            "idC"=>1,
            "idI"=>3,
            "quantite"=>10,
        ]);

        $test =Produit::factory(30)->create();

        // \App\Models\User::factory(10)->create();

            //\App\Models\User::factory(10)->create();
/*
            $moi = new User(["name" => "Moulin Timothée", "email" => "timomoulin@msn.com", "password" => bcrypt("supermdp123")]);
            $moi->save();

            $user2 = User::create(["name" => "foo bar", "email" => "foobar@foo.com", "password" => bcrypt("supermdp123")]);

            $pays1 = new  Pays(["nom" => "egypt"]);
            $pays1->save();

            $voyage1 = new Voyage(["nom" => "Voyage sur le nil", "prix" => 200, "duree" => 7, "pays_id" => 1]);

            $voyage1->save();

            Pays::factory(10)->create();
            */
    }
}

<?php

use App\Http\Controllers\Admin\GrammagesController;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\TypesController;
use App\Http\Controllers\Admin\CommandesController;
use App\Http\Controllers\Admin\AdminProduitController;
use App\Http\Controllers\ClientCommande;
use App\Http\Controllers\ClientCommandeController;
use App\Http\Controllers\ClientPanierController;
use App\Http\Controllers\Controller;
use App\Http\Controllers\Ligne_CommandeController;
use App\Http\Controllers\ProduitController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

//Quand on contacte /produits on declenche la méthode index de ProduitsController
// Route::get('/produits',[ProduitsController::class,"index"]);

// Route::get('/produits/create',[ProduitsController::class,"create"]);

// Route::post('/produits',[ProduitsController::class,"store"]);
// //{produits} corespond a l'id d'un produit
// Route::get('/produits/{produits}',[ProduitsController::class,"show"]);

// Route::get('/produits/{produits}/edit',[ProduitsController::class,"edit"]);

// Route::put('/produits/{produits}',[ProduitsController::class,"update"]);

// Route::delete('/produits/{produits}',[ProduitsController::class,"destroy"]);

//Tout les mappings en seul sur une même syntaxe pour les URL
Route::resource("/produits",ProduitController::class)->parameters(["produit"=>"produit"]);
Route::resource("/types",TypesController::class)->parameters(["types"=>"types"]);
Route::get("/contact", [Controller::class, "afficheForm"]);
Route::post("/contact", [Controller::class, "traitmentForm"]);
Route::get('/reload-captcha', [Controller::class, 'reloadCaptcha']);

Route::middleware("estAdmin")->group(
    function(){
        Route::get('/admin', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
        Route::resource("/admin/types",TypesController::class)->parameters(["type"=>"type"]);
        Route::resource("/admin/grammages",GrammagesController::class)->parameters(["grammage"=>"grammage"]);
        // Route::resource("/admin/lignes",Ligne_CommandeController::class)->parameters(["commande"=>"commande"]);
        Route::resource("/admin/commandes",CommandesController::class)->parameters(["commande"=>"commande"]);
       Route::resource("/admin/produits",AdminProduitController::class)->parameters(["produit"=>"produit"]);
        Route::get("/admin/lignes/{commande}",[Ligne_CommandeController::class,"create"]);
        Route::post("/admin/lignes/{commande}",[Ligne_CommandeController::class,"store"]);


        Route::resource("/grammages",GrammagesController::class)->parameters(["grammages"=>"grammages"]);
        Route::resource("/inventaires",InventairesController::class)->parameters(["inventaires"=>"inventaires"]);
        // Route::resource("/roles",RolesController::class)->parameters(["roles"=>"roles"]);
        // Route::resource("/users",UsersController::class)->parameters(["users"=>"users"]);
    }
);

Route::middleware("auth")->group(
    function(){
  Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::post('/panier',[ClientPanierController::class,"ajoutLigne"]);
Route::get('/panier',[ClientPanierController::class,"consulter"]);
Route::put('/panier/{idInventaire}',[ClientPanierController::class,"modifier"]);
Route::delete('/panier/{idInventaire}',[ClientPanierController::class,"supprimer"]);
Route::delete('/panier',[ClientPanierController::class,"vider"]);
Route::post("/validePanier",[ClientPanierController::class,"valideCommande"]);
Route::resource("/commandes",ClientCommandeController::class)->parameters(["commande"=>"commande"])->only(["index","show"]);

  /* y mettre toutes les routes du client

*/
    }
);

Auth::routes();


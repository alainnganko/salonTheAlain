<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Inventaire extends Model
{
    use HasFactory;
    protected $guarded = ["idI"];
    protected $primaryKey = 'idI';
    public function grammage(){
        return $this->belongsTo(Grammage::class,"idG");
    }
    public function produit(){
        return $this->belongsTo(Produit::class,"idP");
    }

    public function ligneCommandes(){
        return $this->belongsToMany(Commande::class,'ligne_commande',"idI","idC")->using(LigneCommande::class)->withPivot("quantite");
    }

}

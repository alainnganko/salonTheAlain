<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Grammage extends Model
{
    use HasFactory;
    protected $primaryKey = 'idG';
    protected $guarded = ["idG"];
    public function inventaires(){
        return $this->hasMany(Inventaire::class,"idG");
    }
}

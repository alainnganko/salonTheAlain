<?php

namespace App\Models;


use Illuminate\Database\Eloquent\Relations\Pivot;

class LigneCommande extends Pivot
{
    protected $guarded=[];

    public function commande(){
        return $this->belongsTo(Commande::class,"idC");
    }

}

<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $primaryKey = 'idC';
    protected $guarded = ["idC"];

    public function calcTotal(){
        $resultat=0;
        foreach($this->ligneCommandes as $uneLigne){
            $resultat +=$uneLigne->prix * $uneLigne->pivot->quantite;
        }
        return $resultat;
    }

    public function ligneCommandes(){
        return $this->belongsToMany(Inventaire::class,'ligne_commande',"idC","idI")->using(LigneCommande::class)->withPivot("quantite");
    }

    public function users(){
        return $this->belongsTo(User::class,"idUt");
    }

}

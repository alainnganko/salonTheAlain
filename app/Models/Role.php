<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Role extends Model
{
    use HasFactory;
    protected $primaryKey = 'idR';
    protected $guarded = ["idR"];
    public function users(){
        return $this->hasMany(User::class,"idR");
    }
}

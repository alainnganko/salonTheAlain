<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    protected $primaryKey = 'idP';
    protected $guarded = ["idP"];
    public function type(){
        return $this->belongsTo(Type::class,"idT");
    }
    public function inventaires(){
        return $this->hasMany(Inventaire::class,"idP");
    }
}


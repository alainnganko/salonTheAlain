<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use GuzzleHttp\Psr7\Request;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request as HttpRequest;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Mail;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function afficheForm()
    {
       return view("visiteur.contact");
    }


    public function traitmentForm(HttpRequest $request)
    {
        $attributs = $request->validate([
            "email" => "required|string|email",
            "sujet" => "required|string",
            "message" => "required|string",
            'captcha' => 'required|captcha'
        ]);

        //Envoi du mail
        Mail::to("alainnganko@gmail.com")->send(new Contact($attributs["email"], $attributs["sujet"], $attributs["message"]));
        session()->flash("success", "Email envoyé");
        return redirect("/contact");
    }
    public function reloadCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }

}

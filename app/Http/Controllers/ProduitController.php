<?php

namespace App\Http\Controllers;

use App\Models\Produit;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits=Produit::with(["type","inventaires"]);

        $produits=$produits->paginate(16);
       return view("Visiteur.produits.index",["produits"=>$produits]);
    }




    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {

        return view("Visiteur.produits.show",["produit"=>$produit]);
    }






}

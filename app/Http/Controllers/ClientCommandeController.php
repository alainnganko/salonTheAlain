<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use Illuminate\Http\Request;

class ClientCommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $utilisateur=auth()->user();

        $commandes=Commande::select("*")->where("idUt","=",$utilisateur->idUt)->orderBy("dateCom","desc");

         $commandes=$commandes->paginate(10);
        return view("Client.commandes.index",["commandes"=>$commandes]);
    }





    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commande  $commande
     * @return \Illuminate\Http\Response
     */
    public function show(Commande $commande)
    {
        //
    }

}

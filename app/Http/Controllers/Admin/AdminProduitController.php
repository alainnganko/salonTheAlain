<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Produit;
use App\Models\Type;
use Illuminate\Http\Request;

class AdminProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produits=Produit::get();
        return view("Admin.produits.produits",["produits"=>$produits]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $types=Type::all();
        return view("Admin.produits.create",["types"=>$types]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
      $attributs=$request->validate([
        "nomProduit"=>"required|string|min:3|max:255",
        "idT"=>"required|exists:types,idT",
        "marque"=>"required|string|min:2|max:255",
        "duree"=>"required|string|min:2|max:255",
        "image"=>'image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000',
        "estDisponible"=>"",
    ]);
    if(!isset($attributs["estDisponible"]))
    {
        $attributs["estDisponible"]=0;
    }
    else{
        $attributs["estDisponible"]=1;
    }


      $image=$request->file("image")->store("produits");
      $attributs["image"]=$image;
      $produit=Produit::create($attributs);

      session()->flash("Produit ajouter");
      return redirect("/admin/produits");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        $types=Type::all();
        return view("Admin.produits.edit",["types"=>$types]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        $attributs=$request->validate([
            "nomProduit"=>"required|string|min:3|max:255",
            "idT"=>"required|exists:types,idT",
            "marque"=>"required|string|min:2|max:255",
            "duree"=>"required|string|min:2|max:255",
            "image"=>'image|mimes:jpg,png,jpeg,gif,svg|max:2048|dimensions:min_width=100,min_height=100,max_width=1000,max_height=1000',
            "estDisponible"=>"",
        ]);
        if(!isset($attributs["estDisponible"]))
        {
            $attributs["estDisponible"]=0;
        }
        else{
            $attributs["estDisponible"]=1;
        }

        if(isset($attributs["image"])){
            $image=$request->file("image")->store("produits");
            $attributs["image"]=$image;
        }

        $produit->update($attributs);

        session()->flash("Produit ajouter");
      return redirect("/admin/produits");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        //
    }
}

<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\Inventaire;
use App\Models\LigneCommande;
use App\Models\Produit;
use Illuminate\Http\Request;

class Ligne_CommandeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Commande $commande)
    {
       $inventaires=Inventaire::all();
       return view("admin.lignes.create", ["inventaires"=>$inventaires,"commande"=>$commande]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Commande $commande)
    {
        $attributs=$request->validate([
            "quantite"=>"required|numeric|min:1",
            "idI"=>"required|exists:inventaires,idI"
        ]);
        $attributs["idC"]=$commande->idC;
         $ligne=LigneCommande::create($attributs);

        return redirect("/admin/commandes/".$commande->idC);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Ligne_Commande  $ligne_Commande
     * @return \Illuminate\Http\Response
     */
    public function show(LigneCommande $ligne_Commande)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Ligne_Commande  $ligne_Commande
     * @return \Illuminate\Http\Response
     */
    public function edit(LigneCommande $ligne_Commande)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Ligne_Commande  $ligne_Commande
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, LigneCommande $ligne_Commande)
    {
        $attribus = $request->validate(["quantite"=>"required", "prix"=>"required","idUt"=>"required|exists:users,idUt","idI"=>"required|exists:inventaires,idI"]);
        $ligne_Commande=$ligne_Commande->update($attribus);
        session()->flash("success","modification réussie");

        return redirect("/admin/lignes");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Ligne_Commande  $ligne_Commande
     * @return \Illuminate\Http\Response
     */
    public function destroy(LigneCommande $ligne_Commande)
    {
        //
    }
}

<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Grammage;
use Illuminate\Http\Request;

class GrammagesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $grammages=Grammage::get();
        return view("admin.grammages.grammages",["grammages"=>$grammages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view("admin.grammages.create");
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribus = $request->validate(["nbre"=>"required|min:2|max:255", "unite"=>"required|min:2|max:255"]);
        $grammages=Grammage::create($attribus);
        session()->flash("success","création réussie");
        return redirect("/admin/grammages");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Grammage  $grammage
     * @return \Illuminate\Http\Response
     */
    public function show(Grammage $grammage)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Grammage  $grammage
     * @return \Illuminate\Http\Response
     */
    public function edit(Grammage $grammage)
    {
        return view("Admin.grammages.edit",["grammage"=>$grammage]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Grammage  $grammage
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Grammage $grammage)
    {
        $attribus = $request->validate(["nbre"=>"required|min:2|max:255", "unite"=>"required|min:2|max:255"]);
        $grammage=$grammage->update($attribus);
        session()->flash("success","modification réussie");

        return redirect("/admin/grammages");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Grammage  $grammage
     * @return \Illuminate\Http\Response
     */
    public function destroy(Grammage $grammage)
    {
        $grammage->delete();
        session()->flash("success","suppression réussie");
        return redirect("/admin/grammages");
    }
}

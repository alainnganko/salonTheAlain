<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Commande;
use App\Models\User;
use Illuminate\Http\Request;

class CommandesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $commande=Commande::get();
        return view("admin.commandes.commandes",["commandes"=>$commande]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $utilisateurs=User::all();

        return view("admin.commandes.create",["utilisateurs"=>$utilisateurs]);
    }

    /**
     * Store a newly created resource in storage.
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $attribus = $request->validate(["etatCom"=>"required", "dateCom"=>"required", "idUt"=>"required|exists:users,idUt"]);
        $commande=Commande::create($attribus);
        session()->flash("success","création réussie");
        return redirect("/admin/commandes");
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Commandes  $commandes
     * @return \Illuminate\Http\Response
     */
    public function show(Commande $commande)
    {

              return view("Admin.commandes.show",["commande"=>$commande]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Commandes  $commandes
     * @return \Illuminate\Http\Response
     */
    public function edit(Commande $commande)

    {

        $utilisateurs=User::all();

        return view("Admin.commandes.edit",["commande"=>$commande,"utilisateurs"=>$utilisateurs]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Commandes  $commandes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Commande $commande)
    {
        $attribus = $request->validate(["etatCom"=>"required", "dateCom"=>"required","idUt"=>"required|exists:users,idUt"]);
        $commande=$commande->update($attribus);
        session()->flash("success","modification réussie");

        return redirect("/admin/commandes");
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Commandes  $commandes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Commande $commande)
    {
        $commande->delete();
        session()->flash("success","suppression réussie");
        return redirect("/admin/commandes");
    }
}

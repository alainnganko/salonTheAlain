<?php

namespace App\Http\Controllers;

use App\Models\Commande;
use App\Models\Inventaire;
use App\Models\LigneCommande;
use Darryldecode\Cart\Facades\CartFacade;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Date;

class ClientPanierController extends Controller
{
    public function ajoutLigne(Request $request){
        $request->validate([
            "quantite"=>"numeric|required|min:0",
            "idI"=>"required|exists:inventaires,idI"
        ]);
        $inventaire=Inventaire::find($request->input("idI"));

        if(CartFacade::get($inventaire->idI)){
            CartFacade::update($inventaire->idI,["quantity"=>$request->input("quantite")]);
        }
        else{
        CartFacade::add(array(
            'id' => $inventaire->idI,
            'name' => $inventaire->produit->nomProduit,
            'price' => $inventaire->prix,
            'quantity' => $request->input("quantite"),
            'attributes' => array(),
            'associatedModel' => $inventaire
        ));
    }
    session()->flash("success","Produit ajouter au panier");
        return redirect("/panier");

    }

    public function consulter(){
        $items = CartFacade::getContent();

        return view("Client.panier.consulter",["panier"=>$items,"CartFacade"=>CartFacade::class]);
    }

    public function modifier(Request $request,$idInventaire){
        $request->validate([
            "quantite"=>"numeric|required|min:0",
        ]);
        $inventaire=Inventaire::findOrFail($idInventaire);
        if(CartFacade::get($inventaire->idI)){
            CartFacade::update($inventaire->idI,["quantity"=> array(
                'relative' => false,
                'value' => $request->input("quantite")
            )]);
            session()->flash("success","Modification reussie");
        }
        return redirect("/panier");
    }

    public function supprimer($idInventaire){
        if(CartFacade::get($idInventaire)){
            $inventaire=Inventaire::findOrFail($idInventaire);
            CartFacade::remove($idInventaire);
            session()->flash("success","Article retirer du panier");
        }
        else{
            session()->flash("success","Article introuvable");
        }
        return redirect("/panier");
    }

    public function vider(){
        CartFacade::clear();
        session()->flash("success","Panier vider");
        return redirect("/panier");
    }

    public function valideCommande(){
        $panier = CartFacade::getContent();
        if(count($panier)!=0){
            $laCommande=Commande::create([
                "idUt"=>auth()->user()->idUt,
                "etatCom"=>"en préparation",
                "dateCom"=>now(),
            ]);

            foreach($panier as $ligne){
                LigneCommande::create([
                    "idC"=>$laCommande->idC,
                    "idI"=>$ligne->id,
                    "quantite"=>$ligne->quantity,
                ]);
            }
            session()->flash("success","Commande envoyer");
            CartFacade::clear();
            return redirect("/commandes");
        }
    }
}

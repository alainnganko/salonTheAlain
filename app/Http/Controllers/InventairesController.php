<?php

namespace App\Http\Controllers;
use App\Models\Inventaire;
use Illuminate\Http\Request;

class InventairesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Inventaires  $inventaires
     * @return \Illuminate\Http\Response
     */
    public function show(Inventaire $inventaires)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Inventaires  $inventaires
     * @return \Illuminate\Http\Response
     */
    public function edit(Inventaire $inventaires)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Inventaires  $inventaires
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Inventaire $inventaires)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Inventaires  $inventaires
     * @return \Illuminate\Http\Response
     */
    public function destroy(Inventaire $inventaires)
    {
        //
    }
}

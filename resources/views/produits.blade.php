<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Produits Thé</title>
</head>
<body>
@foreach ($produits as $nomProduit )
{{$nomProduit->nomProduit}}
@endforeach

@foreach ($produits as $marque )
{{$marque->marque}}
@endforeach

@foreach ($produits as $image )
{{$image->image}}
@endforeach

@foreach ($produits as $prix)
{{$prix->prix}}
@endforeach

@foreach ($produits as $duree )
{{$duree->duree}}
@endforeach
@foreach ($produits as $estDisponible )
{{$estDisponible->estDisponible}}
@endforeach

</body>
</html>

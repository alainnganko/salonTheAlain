@extends("template")
@section("titre")
{{$produit->nomProduit}}
@endsection
@section("content")
<div class="container">
    <h1>{{ Str::upper($produit->nomProduit)}}</h1>

    <section>
       <h2>Les informations</h2>

       <b>Parfum </b> : {{$produit->marque}} <br>
    </section>
    @if (Auth::user())


    <section>
        <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h2>Ajouter a votre panier</h2>
        <form action="/panier" method="post">
        @csrf
        <div class='row mb-2'>
            <label for='quantite'>Quantite *</label>
                <input value='{{old("quantite")}}' name='quantite' required type='text' class="form-control" id="quantite" placeholder="Enter quantite">
            @error('quantite')
                <div class='alert alert-danger mt-1'>{{$message}}</div>
            @enderror
        </div>

        <div class='row mb-2'>
            <label for='idI'>Choix *</label>
                <select value='{{old("idI")}}' name='idI' required class="form-control" id="idI" >
                    <option value="" selected disabled>Choisir un Grammage</option>
                    @foreach ($produit->inventaires as $unInventaire )
                    <option value="{{$unInventaire->idI}}">{{$unInventaire->grammage->nbre}} {{$unInventaire->grammage->unite}}</option>
                    @endforeach
                </select>
            @error('idI')
                <div class='alert alert-danger mt-1'>{{message}}</div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Ajouter au panier</button>
        </form>
        </div>
        </div>
    </section>
    @endif
</div>
@endsection

@extends("template")
@section("titre")
Produits
@endsection
@section("content")
    <h1>Les produits</h1>
    {{$produits->links()}}
    <div class="row row-cols-2">

        @foreach ($produits as $unProduit )

        <div class="card col" style="width: 18rem;">
            <img src="/storage/{{$unProduit->image ?? "produits/defaut.jpg"}}" class="card-img-top" alt="...">
            <div class="card-body">
              <h5 class="card-title">{{Str::upper( $unProduit->nomProduit)}}</h5>
              <p class="card-text">

                <b>Parfum </b> : {{$unProduit->marque}} <br>
              </p>
              @if ($unProduit->estDisponible)
                   <div class="alert alert-success">
                  Disponible
              </div>
              @else
                 <div class="alert alert-danger">
                Indisponible
            </div>
              @endif
              <a href="/produits/{{$unProduit->idP}}" class="btn btn-primary">Consulter</a>
            </div>
          </div>

          @endforeach
    </div>
    {{$produits->links()}}
@endsection

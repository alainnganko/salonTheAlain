@extends("template")
@section("titre")
Commandes
@endsection
@section("content")
<h1>Les Commandes de thé</h1>
<a href="/admin/commandes/create" class="btn btn-primary my-2">Ajouter</a><br>
<table class="table">
    <thead>
        <th>Etat Commandes</th>
        <th>Date Commandes</th>
        <th>Client</th>
        <th>Action</th>
    </thead>


    <tbody>
        @foreach ($commandes as $commande )
        <tr>
            <td>{{$commande->etatCom}}</td>
            <td>{{$commande->dateCom}}</td>
            <td>{{$commande->users->nomUt}} {{$commande->users->prenomUt}}</td>
            <td>
                @if (auth()->user()->role->nomRole=="Admin")
                <a href="/admin/commandes/{{$commande->idC}}/edit" class="btn btn-secondary mb-2">Modifier</a>
                <form action="/admin/commandes/{{$commande->idC}}" method="POST">
                    @csrf
                    @method("delete")
                    <button class="btn btn-danger">Suprimer</button>
                </form>
                @endif

                <a href="/commandes/{{$commande->idC}}" class="btn btn-secondary mb-2">Consulter</a>
            </td>
        </tr>

        @endforeach
    </tbody>
</table>

@endsection

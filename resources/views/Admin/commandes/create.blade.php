@extends("template")
@section("titre")
Commandes de Thé
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Commandes de Thé</h1>

        <form action="/admin/commandes" method="post" enctype="multipart/form-data">

            @csrf
            <div class="row mb-2">
                <label for="etatCom">etatCom *</label>
                <input value="{{old("etatCom")}}" name="etatCom" required type="text" class="form-control" id="etatCom" placeholder="Enter etatCom">
            @error("etatCom")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="dateCom"> Date *</label>
                <input value="{{old("dateCom")}}" name="dateCom" required type="date" class="form-control" id="dateCom" placeholder="Enter dateCom">
            @error("dateCom")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="idUt"> Client *</label>
<select name="idUt" id="idUt" class="form-control">
    <option value="" disabled selected>Choisir un client</option>
    @foreach ($utilisateurs as $unUtilisateur )
    <option value="{{$unUtilisateur->idUt}}">{{$unUtilisateur->nomUt}} {{$unUtilisateur->prenomUt}} </option>

    @endforeach
</select>
            @error("idUt")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection


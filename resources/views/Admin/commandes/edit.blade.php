@extends("template")
@section("titre")
Commandes de Thé
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Commandes de Thé</h1>

        <form action="/admin/commandes/{{$commande->idC}}" method="post" enctype="multipart/form-data">
@method("put")
            @csrf
            <div class="row mb-2">
                <label for="etatCom">etatCom *</label>
                <input value="{{old("etatCom")??$commande->etatCom}}" name="etatCom" required type="text" class="form-control" id="etatCom" placeholder="Enter etatCom">
            @error("nomType")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="dateCom"> Date </label>
                <input value="{{old("dateCom")??$commande->dateCom}}" name="dateCom" required type="date" class="form-control" id="dateCom" placeholder="Enter dateCom">
            @error("dateCom")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="idUt"> Client *</label>
<select name="idUt" id="idUt" class="form-control">
    @foreach ($utilisateurs as $unUtilisateur )
    <option selected="{{$unUtilisateur->idUT==$commande->users->idUt}}" value="{{$unUtilisateur->idUt}}">{{$unUtilisateur->nomUt}} {{$unUtilisateur->prenomUt}} </option>

    @endforeach
</select>
            @error("idUt")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>
        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

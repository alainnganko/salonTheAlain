@extends("template")
@section("titre")
Show Commandes
@endsection
@section("content")
<h1>Commandes </h1>
<a href="/admin/lignes/{{$commande->idC}}" class="btn btn-primary my-2">Ajouter</a><br>
<table class="table">
    <thead>
        <th>Nom thé</th>
        <th>Prix du thé</th>
        <th>Quantité de thé</th>
        <th>Action</th>
    </thead>


<tbody>

    @foreach ($commande->ligneCommandes as $ligne )
    <tr>
        <td>{{$ligne->produit->nomProduit}}</td>
        <td>{{$ligne->produit->prix}}</td>
        <td>{{$ligne->pivot->quantite}}</td>
        <td>
{{--
<a href="/admin/commandes/{{$ligne->idC}}/edit" class="btn btn-secondary mb-2">Modifier</a>
<form action="/admin/commandes/{{$ligne->idC}}" method="POST">
@csrf
@method("delete")
<button class="btn btn-danger">Suprimer</button>
</form> --}}
        </td>
    </tr>

    @endforeach
</tbody>
</table>

@endsection


@extends("template")
@section("titre")
Grammages de Thé
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Grammages de Thé</h1>

        <form action="/admin/grammages" method="post" enctype="multipart/form-data">

            @csrf
            <div class="row mb-2">
                <label for="nbre">{!! Form::number($name, $value, [$options]) !!} *</label>
                <input value="{{old("nbre")}}" name="nbre" required type="number" class="form-control" id="nbre" placeholder="Enter nbre">
            @error("nbre")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="unite">unite *</label>
                <input value="{{old("unite")}}" name="unite" required type="text" class="form-control" id="unite" placeholder="Enter unite">
            @error("unite")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>


        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

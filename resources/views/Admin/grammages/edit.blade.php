@extends("template")
@section("titre")
Grammages de thé
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Grammages de Thé</h1>

        <form action="/admin/grammages/{{$grammage->idG}}" method="post" enctype="multipart/form-data">
@method("put")
            @csrf
            <div class="row mb-2">
                <label for="nbre">{!! Form::number($name, $value, [$options]) !!} *</label>
                <input value="{{old("nbre")??$grammage->nbre}}" name="nbre" required type="number" class="form-control" id="nbre" placeholder="Enter nbre">
            @error("nbre")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="couleurType">unite *</label>
                <input value="{{old("unite")??$grammage->unite}}" name="unite" required type="text" class="form-control" id="unite" placeholder="Enter unite">
            @error("unite")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>


        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

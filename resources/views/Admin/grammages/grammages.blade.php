@extends("template")
@section("titre")
Grammages
@endsection
@section("content")
<h1>Grammages de thé</h1>
<a href="/admin/grammage/create" class="btn btn-primary my-2">Ajouter</a><br>
<table class="table">
    <thead>
        <th>Nombre</th>
        <th>Unité</th>
        <th>Action</th>
    </thead>


<tbody>
    @foreach ($grammages as $grammage )
    <tr>
        <td>{{$grammage->nbre}}</td>
        <td>{{$grammage->unite}}</td>
        <td>
<a href="/admin/grammages/{{$grammage->idG}}/edit" class="btn btn-secondary mb-2">Modifier</a>
<form action="/admin/grammages/{{$grammage->idG}}" method="POST">
@csrf
@method("delete")
<button class="btn btn-danger">Suprimer</button>
</form>
        </td>
    </tr>

    @endforeach
</tbody>
</table>

@endsection


@extends('template')
@section('titre')
    Lignes Commandes
@endsection

@section('content')
    <div class="container">
        <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
            <h1 class="my-1">Lignes Commandes</h1>

            <form action="/admin/lignes/{{ $commande->idC }}" method="post" enctype="multipart/form-data">
                <div class="row mb-2">
                    <label for="idI"> Inventaire *</label>
                    <select name="idI" id="idI" class="form-control">
                        <option value="" disabled selected>Choix de l'Inventaire</option>
                        @foreach ($inventaires as $unInventaire)
                            <option value="{{ $unInventaire->idI }}">{{ $unInventaire->produit->nomProduit }}
                                {{ $unInventaire->grammage->nbre }} {{ $unInventaire->grammage->unite }}</option>
                        @endforeach
                    </select>
                    @error('idUt')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>

                @csrf
                <div class="row mb-2">
                    <label for="nomType">Quantité *</label>
                    <input value="{{ old('quantite') }}" name="quantite" required type="number" class="form-control"
                        id="quantite" placeholder="Enter quantite">
                    @error('quantite')
                        <div class="alert alert-danger mt-1">{{ $message }}</div>
                    @enderror
                </div>


                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
@endsection

@extends("template")
@section("titre")
Types
@endsection
@section("content")
<h1>Les Types de thé</h1>
<a href="/admin/types/create" class="btn btn-primary my-2">Ajouter</a><br>
<table class="table">
    <thead>
        <th>Nom</th>
        <th>Couleur</th>
        <th>Action</th>
    </thead>


<tbody>
    @foreach ($lesTypes as $unType )
    <tr>
        <td>{{$unType->nomType}}</td>
        <td>{{$unType->couleurType}}</td>
        <td>
<a href="/admin/types/{{$unType->idT}}/edit" class="btn btn-secondary mb-2">Modifier</a>
<form action="/admin/types/{{$unType->idT}}" method="POST">
@csrf
@method("delete")
<button class="btn btn-danger">Suprimer</button>
</form>
        </td>
    </tr>

    @endforeach
</tbody>
</table>

@endsection


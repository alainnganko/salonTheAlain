@extends("template")
@section("titre")
Type de Thé
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Type de Thé</h1>

        <form action="/admin/types/{{$unType->idT}}" method="post" enctype="multipart/form-data">
@method("put")
            @csrf
            <div class="row mb-2">
                <label for="nomType">nomType *</label>
                <input value="{{old("nomType")??$unType->nomType}}" name="nomType" required type="text" class="form-control" id="nomType" placeholder="Enter nomType">
            @error("nomType")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="couleurType">couleurType *</label>
                <input value="{{old("couleurType")??$unType->couleurType}}" name="couleurType" required type="text" class="form-control" id="couleurType" placeholder="Enter couleurType">
            @error("couleurType")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>


        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

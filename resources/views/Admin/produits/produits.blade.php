@extends("template")
@section("titre")
Gestion des Produits
@endsection
@section("content")
<h1>Gestion des Produits </h1>
<a href="/admin/types/create" class="btn btn-primary my-2">Ajouter</a><br>
<table class="table">
    <thead>
        <th>Nom</th>
        <th>Parfum</th>
        <th>Conservation</th>
        <th>Disponibilté</th>
        <th>Image</th>
        <th>Action</th>
    </thead>


<tbody>
    @foreach ($produits as $unProduit )
    <tr>
        <td>{{$unProduit->nomProduit}}</td>
        <td>{{$unProduit->marque}}</td>
        <td>{{$unProduit->duree}}</td>
        <td>{{$unProduit->estDisponible}}</td>
        <td>{{$unProduit->image}}</td>
        <td>
<a href="/admin/produits/{{$unProduit->idP}}/edit" class="btn btn-secondary mb-2">Modifier</a>
<form action="/admin/produits/{{$unProduit->idP}}" method="POST">
@csrf
@method("delete")
<button class="btn btn-danger">Suprimer</button>
</form>
        </td>
    </tr>

    @endforeach
</tbody>
</table>

@endsection


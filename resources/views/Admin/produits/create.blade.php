@extends("template")
@section("titre")
Ajout d'un produit
@endsection
@section("content")
<div class="container">
<div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
<h1 class="my-1">Ajout Produit</h1>
<form action="/admin/produits" method="post" enctype="multipart/form-data">
@csrf

<div class='row mb-2'>
    <label for='nomProduit'>Nom *</label>
        <input value='{{old("nomProduit")}}' name='nomProduit' required type='text' class="form-control" id="nomProduit" placeholder="Nom du produit">
    @error('nomProduit')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<div class='row mb-2'>
    <label for='marque'>Parfum *</label>
        <input value='{{old("marque")}}' name='marque' required type='text' class="form-control" id="marque" placeholder="Enter parfum">
    @error('marque')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<div class='row mb-2'>
    <label for='duree'>Conservation *</label>
        <input value='{{old("duree")}}' name='duree' required type='text' class="form-control" id="duree" placeholder="Enter duree">
    @error('duree')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<div class='row mb-2'>
    <label for='estDisponible'>Disponible *</label>
        <input value='{{old("estDisponible")}}' name='estDisponible'  type='checkbox'  id="estDisponible" >
    @error('estDisponible')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<div class='row mb-2'>
    <label for='image'>image *</label>
        <input value='{{old("image")}}' name='image'  type='file' class="form-control" id="image" placeholder="Enter image">
    @error('image')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<div class='row mb-2'>
    <label for='idT'>Type *</label>
        <select value='{{old("idT")}}' name='idT' required  class="form-control" id="idT" >
            <option value="" disabled selected>Choisir le type</option>
            @foreach ($types as $unType )
                <option value="{{$unType->idT}}">{{$unType->nomType}} {{$unType->couleurType}} </option>
            @endforeach
        </select>
    @error('idT')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
</div>

<button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</div>
@endsection

@extends("template")
@section("titre")
Produits Admin
@endsection

@section("content")
<div class="container">
    <div class="col-12 col-sm-10 col-md-6 col-lg-4 mx-auto">
        <h1 class="my-1">Produits Admin</h1>

        <form action="/admin/types/{{$unProduit->idP}}" method="post" enctype="multipart/form-data">
@method("put")
            @csrf
            <div class="row mb-2">
                <label for="nomProduit">nomProduit *</label>
                <input value="{{old("nomProduit")??$unProduit->nomProduit}}" name="nomProduit" required type="text" class="form-control" id="nomProduit">
            @error("nomProduit")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="marque">Parfum *</label>
                <input value="{{old("marque")??$unProduit->marque}}" name="marque" required type="text" class="form-control" id="marque" placeholder="Enter marque">
            @error("marque")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="duree">Conservation *</label>
                <input value="{{old("duree")??$duree->duree}}" name="duree" required type="date" class="form-control" id="duree">
            @error("duree")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="estDisponible">Disponibilité *</label>
                <input value="{{old("estDisponible")??$estDisponible->estDisponible}}" name="estDisponible" required type="checkbox" class="form-control" id="estDisponible">
            @error("estDisponible")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>

            <div class="row mb-2">
                <label for="image">image *</label>
                <input value="{{old("image")??$image->image}}" name="image" required type="text" class="form-control" id="image">
            @error("image")
                <div class="alert alert-danger mt-1">{{$message}}</div>
            @enderror
            </div>


        <button type="submit" class="btn btn-primary">Submit</button>
        </form>
    </div>
</div>
@endsection

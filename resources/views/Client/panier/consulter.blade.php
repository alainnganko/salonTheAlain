@extends("template")
@section("titre")
Votre Panier
@endsection
@section("content")
<h1>Votre panier</h1>

<table class="table table-responsive">
    <thead>
    <tr>
        <th>Nom</th>
        <th>Grammage</th>
        <th>Prix Unitaire</th>
        <th>Quantite</th>
        <th>Sous-Total</th>
        <th>Action</th>
    </tr>
</thead>
    <tbody>
        @foreach ($panier as $uneLigne )
        <tr>
            <td>{{$uneLigne->name}}</td>
            <td>{{$uneLigne->associatedModel->grammage->nbre}} {{$uneLigne->associatedModel->grammage->unite}} </td>
            <td>{{$uneLigne->price}}€</td>
            <td>{{$uneLigne->quantity}}</td>
            <td>{{$CartFacade::getSubTotal($uneLigne->id)}}</td>
            <td>
                <form action="/panier/{{$uneLigne->id}}" method="post">
                    @csrf
                    @method("delete")
                    <button type="button" class="btn btn-primary  pb-2" data-bs-toggle="modal"
                        data-bs-target="#ligne{{$uneLigne->id}}" data-bs-whatever="@mdo">Modifier</button>
                    <button class="btn btn-danger">Supprimer la ligne</button>
                </form>
            </td>
        </tr>

        <div class="modal fade" id="ligne{{$uneLigne->id}}" tabindex="-1" aria-labelledby="ligne{{$uneLigne->id}}Label"
            aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="ligne{{$uneLigne->id}}Label">Modifiation</h5>
                        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                    </div>
                    <div class="modal-body">
                        <form action="/panier/{{$uneLigne->id}}" method="post">
                            @csrf
                            @method("put")
                            <div class="mb-3">
                                <label for="inputQuantite" class="col-form-label">Quantite</label>
                                <input name="quantite" class="form-control" value="{{$uneLigne->quantity}}"
                                    type="number" required min="1" class="form-control" id="inputQuantite">
                            </div>
                            <button class="btn btn-primary">Envoyer</button>
                        </form>
                    </div>

                </div>
            </div>
        </div>
        @endforeach
        <tr>
            <td>_</td>
            <td>_</td>
            <td>_</td>
            <td>_</td>
            <td>{{$CartFacade::getTotal()}}</td>
            <td>_</td>
        </tr>
    </tbody>
</table>
<div class="row">
    <form action="/panier" method="post" class="col-2 col-lg-1">
        @csrf
        @method("delete")
        <button class="btn btn-danger">Vider panier</button>
    </form>

    <form action="/validePanier" method="POST" class="col-2 col-lg-1">
        @csrf
        <button class="btn btn-success">Valider panier</button>
    </form>
</div>
@endsection

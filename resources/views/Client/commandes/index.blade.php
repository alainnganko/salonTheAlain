@extends("template")
@section("titre")
Mes Commandes
@endsection
@section("content")
<h1>Mes Commandes</h1>
{{$commandes->links()}}
<table class="table">
    <thead>
        <tr>
            <th>Numero</th>
            <th>Date</th>
            <th>Etat</th>
            <th>Prix</th>
            <th>Actions</th>
        </tr>
        <tbody>
            @foreach ($commandes as $uneCommande )
            <tr>
                <td>{{$uneCommande->idC}}</td>
                <td>{{$uneCommande->dateCom}}</td>
                <td>{{$uneCommande->etatCom}}</td>
                <td>{{$uneCommande->calcTotal()}}</td>
                <td>

                </td>
            </tr>
            @endforeach
        </tbody>
    </thead>
</table>
@endsection
